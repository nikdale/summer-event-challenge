# Summer Event Challenge

Check if `.env` file is there. If not, please, create one from `.env.example`.
Of course, first, go with `npm i` in both server and client.

To run the apps in local, go to location of app you want to start and use `npm start` to run it.

To start the services inside Docker, go with `docker-compose -up -d`
Look up for the Client up at *localhost:3000*


## System Monitoring

* Access `Prometheus` at http://localhost:9090/
* Access `cAdvisor` at http://localhost:9080/

## Central logging 

* Access `Kibana` at http://localhost:5601
* Now configure the ElasticSearch indices in Kibana. Go over to **Create Index Pattern** and create one for `filebeat-*`.

### Some of `Prometheus` expressions:
* `container_start_time_seconds`
* `container_memory_usage_bytes{name="scanner"}`
* `rate(container_network_transmit_bytes_total[1m])`
