import React from 'react';
import './App.css';
import SearchConnection from './components/searchConnection';


export class ShowData extends React.Component {

  constructor(props) {
    super(props);
    this.state = { data: '', sectionsDataArray: [] };

    this.handleDataReceived = this.handleDataReceived.bind(this);
  }

  diff_minutes = (dt2, dt1) => {
    let diff = (dt2.getTime() - dt1.getTime()) / 1000;
    diff /= 60;
    return Math.abs(Math.round(diff));
  };

  timeConvert = (num) => {
    const hours = (num / 60);
    const diffHours = Math.floor(hours);
    const minutes = (hours - diffHours) * 60;
    const diffMin = Math.round(minutes);
    return { diffHours, diffMin };
  }

  handleDataReceived = (dataReceived) => {
    this.setState({ data: dataReceived });
    const dataToProcess = dataReceived.connections[0].sections;
    const sectionsArray = [];
    if (dataToProcess) {
      dataToProcess.forEach(section => {
        let sectionData = {};
        if (section.walk) {
          sectionData.type = 'walk';
        };
        if (section.journey) {
          sectionData.type = 'journey';
          sectionData.transport = section.journey.category + section.journey.number;
        };
        sectionData.arrival = section.arrival.location.name;

        const dt1 = new Date(section.arrival.arrival),
          dt2 = new Date(section.departure.departure);

        sectionData.diffMinutes = this.diff_minutes(dt1, dt2);
        if (sectionData.diffMinutes > 59) {
          sectionData.diffHours = this.timeConvert(sectionData.diffMinutes);
        };

        sectionsArray.push(sectionData);
      });
      this.setState({ sectionsDataArray: sectionsArray });
    };

  };

  render() {

    let sectionsData = this.state.sectionsDataArray.map((section, i) => {
      let time = '';
      if (section.diffHours) {
        if (section.diffHours.diffHours) {
          time = `${section.diffHours.diffHours}h `
        };
        if (section.diffHours.diffMin) {
          time = time + `${section.diffHours.diffMin}m`;
        };
      } else {
        time = `${section.diffMinutes}m`
      };

      if (section.type === 'journey') {
        return (
          <tr key={i}>
            <td>Take {section.transport} to {section.arrival}</td>
            <td>{time}</td>
          </tr>
        );
      } else if (section.type === 'walk') {
        return (
          <tr key={i}>
            <td>Walk to {section.arrival}</td>
            <td>{time}</td>
          </tr>
        );
      };

      return '??';
    });


    return (
      <div className="App">
        <header className="App-header">
          <SearchConnection onReceivedData={this.handleDataReceived} />
          <div className="table-responsive">
            <table id="mytable" className="table table-bordred table-striped">
              <tbody>
                {sectionsData}
              </tbody>
            </table>
          </div>
        </header>
      </div>
    );
  }
}

function App() {
  return (
    <ShowData />
  )
}

export default App;
