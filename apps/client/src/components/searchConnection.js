import React from 'react';
const fetch = require("node-fetch");
const serverPort = process.env.SERVER_PORT || 4000;

export default class SearchConnection extends React.Component {
    constructor(props) {
        super(props);
        this.state = { from: '', to: '', limitConenctions: 4, time: '17:15' };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { id, value } = e.target;
        if (e.target.pattern) {
            this.setState({ [id]: parseInt(value) ? parseInt(value) : '' })
        } else {
            this.setState({ [id]: value });
        };
    }

    async handleSubmit(e) {
        e.preventDefault();

        const params = this.state;
        try {
            const requestOptions = {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                }
            };
            const url = new URL(`http://localhost:${serverPort}/getConnections`);
            Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
            await fetch(url, requestOptions)
                .then(handleResponse)
                .then(response => {
                    this.props.onReceivedData(response);
                });
        } catch (error) {
            console.log(error);
            alert(error.message);
        };
    };

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                    <label htmlFor="from">From:</label>
                    <input type="text" id="from" required value={this.state.from} onChange={this.handleChange} />
                </div>
                <div className="form-group">
                    <label htmlFor="from">To:</label>
                    <input type="text" id="to" required value={this.state.to} onChange={this.handleChange} />
                </div>

                <div className="form-group">
                    <label htmlFor="limitConenctions">Number of connections: </label>
                    <input type="number" required id="limitConenctions" min="1" max="16" value={this.state.limitConenctions} onChange={this.handleChange} />
                </div>
                <input type="time" required className="form-control" id="time" value={this.state.time} onChange={this.handleChange} />
                <input type="submit" value="Search" />
            </form>
        );
    };
};


function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401 || response.status === 400) {
            };
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
};