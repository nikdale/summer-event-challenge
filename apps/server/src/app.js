const express = require('express');
const app = express();
const fetch = require("node-fetch");
const cors = require('cors');
const port = process.env.SERVER_PORT || 4000;


app.use(cors());

/**
 * @api {get} /getConnections Request a connections data
 *
 * @apiParam {from} string, place from
 * @apiParam {to} string, place to
 * @apiParam {time} string, arrival time, example: 12:20
 * @apiParam {limit} connections limit, from 1 to 16, default 4
 */
app.get('/getConnections', async (req, res) => {
  try {
      const params = { from: req.query.from, to: req.query.to, time: req.query.time, isArrivalTime: 1, limit: req.query.limitConenctions };
      if (params.limit === undefined) {
        params.limit = 4;
      };
      if (params.time === undefined) {
        params.time = '17:15';
      };

      const requestOptions = {
          method: 'GET',
          headers: { 
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*',
          }
      };
      const url = new URL("http://transport.opendata.ch/v1/connections");
      Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

      await fetch(url, requestOptions)
      .then(handleResponse)
      .then(response => {
        return res.status(201).json(response);
      });

  } catch (error) {
    res.status(500).json({
      errors: [
        {
          title: 'Error getting connections',
          detail: 'Something went wrong during connections fetch.',
          errorMessage: error.message,
        },
      ],
    });
  }
});

app.listen(port, () => {
  console.log(`Server running at: http://localhost:${port}/`);
});

function handleResponse(response) {
  return response.text().then(text => {
      const data = text && JSON.parse(text);
      if (!response.ok) {
          if (response.status === 401 || response.status === 400) {
          };
          const error = (data && data.message) || response.statusText;
          return Promise.reject(error);
      }

      return data;
  });
};

module.exports = { app };
